provider "aws" {
    region = "us-east-2"
  
}
resource "aws_kms_key" "terraform-backend-key" {
  description             = "This key is used to encrypt terraform-backend"
  deletion_window_in_days = 10
}

resource "aws_s3_bucket" "terraform-backend" {
    bucket = var.bucket_name
    tags = var.tags
}

resource "aws_s3_bucket_acl" "terraform-backend" {
  bucket = aws_s3_bucket.terraform-backend.id
    acl = var.acl
}

resource "aws_s3_bucket_server_side_encryption_configuration" "terrafom-backend" {
  bucket = aws_s3_bucket.terraform-backend.bucket

  rule {
    apply_server_side_encryption_by_default {
      kms_master_key_id = aws_kms_key.terraform-backend-key.arn
      sse_algorithm     = "aws:kms"
    }
  }
}

output "kms_arn" {
    value = aws_kms_key.terraform-backend-key.arn
}