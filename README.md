# Summary

This project contains a CI/CD pipeline using Gitlab and IAC code using  terraform  to create the resources in AWS.

## Resources in aws
API Gateway   
Lambda  
Cognito (Created and configured, pending to associate to the GET method for maintaining easy tests. )  
S3 (terraform backend) 
IAM Role

![](images/scenario-api-gateway.png)


## Getting started

Create AWS resources

```
#create backend
cd 01-bucket-to-backend-s3
terraform init 
terraform apply -auto-approve

#create aws resources for the api
cd 02-aws-api-lambda-app
terraform init 
terraform apply -auto-approve
```

## CI/CD

The pipeline contains two jobs, test and deploy. The first one, test all actions in the  `api.py` function, the second one, deploy an update  the  lambda function using AWS CLI tool.


## Endpoint API Method

[Link to API](https://lmibn16i6c.execute-api.us-west-2.amazonaws.com/dev/math_operations?action=square&number=100)


##  Endpoint API Method Cognito
[Link to API](https://math-domain.auth.us-west-2.amazoncognito.com/login?client_id=77oou35760mtpur9tmnmnf7d3e&response_type=token&scope=openid+profile&redirect_uri=http://localhost:8000/callback)





