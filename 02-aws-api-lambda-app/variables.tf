variable "project_name" {
  default     = "math-operations"
  description = "The name of the project"
}   

variable "api_name" {
  default     = "API Math Operations"
  description = "The name of the api"
  
}

variable "pool_name" {
  default     = "MathPool"
  description = "The name of the api"
  
}

variable "domain" {
  default     = "math-domain"
  description = "The name of the api"
  
  
}

variable "region_name" {
    default = "us-west-2"
  
}

variable "bucket_name" {
  
}

variable "acl" {
    default = "private"
  
}

variable "tags" {
  
}

variable "s3_object_key" {
  
}

variable "account_id" {
  
}

variable "function_name" {

}