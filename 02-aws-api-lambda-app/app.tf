module "aws-app" {
    source = "./modules/app"
    bucket_name = var.bucket_name
    acl = var.acl
    tags = var.tags
    s3_object_key = var.s3_object_key
    project_name = var.project_name
    api_name = var.api_name
    pool_name = var.pool_name
    domain = var.domain
    account_id = var.account_id
    function_name = var.function_name
    
}
output "invoke_url" {
    value = module.aws-app.invoke_url
}


