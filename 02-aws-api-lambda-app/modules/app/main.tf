provider "aws" {
    region = var.region_name  
}

#create bucket for lambda code
resource "aws_s3_bucket" "bucket" {
    bucket = var.bucket_name
    tags = var.tags
}

resource "aws_s3_bucket_acl" "acl" {
    bucket = aws_s3_bucket.bucket.id
    acl = var.acl
}


data "archive_file" "lambda_zip" {
    type = "zip"

    source_dir = "${path.module}/code"
    output_path = "${path.module}/code/api.zip"
}
#upload code in zip to bucker
resource "aws_s3_object" "lambda_object" {
    bucket = "${aws_s3_bucket.bucket.id}"
    key = var.s3_object_key
    source = "${data.archive_file.lambda_zip.output_path}"
}

#create lambda function 
resource "aws_lambda_function" "api" {
    function_name = var.function_name

    s3_bucket = aws_s3_bucket.bucket.id
    s3_key = aws_s3_object.lambda_object.key

    handler = "api.lambda_handler"
    runtime = "python3.9"
    
    role = aws_iam_role.lambda_exec.arn
  
}
#create the execution role for lambda function
resource "aws_iam_role" "lambda_exec" {
    name = "api_lambda_execution_role"
    assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  
}

#create cognito pool
resource "aws_cognito_user_pool" "pool" {
  name = var.pool_name
  schema {
    name                = "email"
    attribute_data_type = "String"
    required            = true    
    mutable = false
    string_attribute_constraints {
      min_length = 1
      max_length = 256
    }
  }

  username_configuration {
    case_sensitive = false
  }

  account_recovery_setting {
    recovery_mechanism {
      name     = "verified_email"
      priority = 1
    }

    
  }
  lifecycle {

    ignore_changes = [
      password_policy,
      schema
    ]
  }
}

resource "aws_cognito_user_pool_domain" "main" {
  domain       = var.domain
  user_pool_id = aws_cognito_user_pool.pool.id
}

resource "aws_cognito_user_pool_client" "client" {
  name = "App Client Math App"

  user_pool_id = aws_cognito_user_pool.pool.id
  callback_urls = [
    "http://localhost:8000/callback",
  ]
  logout_urls = [ "http://localhost:8000/logout" ]
  allowed_oauth_flows = ["implicit"]
  allowed_oauth_scopes = ["openid", "profile"]
  allowed_oauth_flows_user_pool_client = true
  supported_identity_providers = ["COGNITO"]
}

#create api gateway
resource "aws_api_gateway_rest_api" "math_api" {
  body = jsonencode({
    openapi = "3.0.1"
    info = {
      title   = "${var.project_name}-api"
      version = "1.0"
    }
   
  })

  name = "${var.api_name}"

  endpoint_configuration {
    types = ["REGIONAL"]
  }
}


resource "aws_api_gateway_resource" "resource" {
  rest_api_id = "${aws_api_gateway_rest_api.math_api.id}"
  parent_id   = "${aws_api_gateway_rest_api.math_api.root_resource_id}"
  path_part   = "math_operations"
}

resource "aws_api_gateway_method" "method" {
  rest_api_id   = "${aws_api_gateway_rest_api.math_api.id}"
  resource_id   = "${aws_api_gateway_resource.resource.id}"
  http_method   = "GET"
  authorization = "NONE"
  
}

resource "aws_api_gateway_integration" "integration" {
  rest_api_id             = aws_api_gateway_rest_api.math_api.id
  resource_id             = aws_api_gateway_resource.resource.id
  http_method             = aws_api_gateway_method.method.http_method
  integration_http_method = "POST"
  type                    = "AWS"
  uri                     = aws_lambda_function.api.invoke_arn
  request_templates = {
  "application/json" = <<EOF
{
    "action" : "$util.escapeJavaScript($input.params('action'))",
    "number" : $util.escapeJavaScript($input.params('number'))       
}
EOF
  }
  
}

resource "aws_api_gateway_method_response" "response_200" {
  rest_api_id = aws_api_gateway_rest_api.math_api.id
  resource_id = aws_api_gateway_resource.resource.id
  http_method = aws_api_gateway_method.method.http_method
  status_code = "200"
  response_parameters = { "method.response.header.Access-Control-Allow-Origin" = true }
}

resource "aws_api_gateway_integration_response" "integration_response_200" {
  rest_api_id = aws_api_gateway_rest_api.math_api.id
  resource_id = aws_api_gateway_resource.resource.id
  http_method = aws_api_gateway_method.method.http_method
  status_code = aws_api_gateway_method_response.response_200.status_code
  response_parameters = { "method.response.header.Access-Control-Allow-Origin" = "'*'" }
  
}



resource "aws_api_gateway_deployment" "deployment" {
  rest_api_id = aws_api_gateway_rest_api.math_api.id
  stage_name = "dev"
  stage_description = "Deployed at ${timestamp()}"
  triggers = {
     redeployment = sha1(jsonencode([
      aws_api_gateway_resource.resource.id,
      aws_api_gateway_method.method.id,
      aws_api_gateway_integration.integration.id,
    ]))
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_lambda_permission" "apigw" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.api.function_name}"
  principal     = "apigateway.amazonaws.com"

  # The /*/* portion grants access from any method on any resource
  # within the API Gateway "REST API".
  source_arn = "${aws_api_gateway_rest_api.math_api.execution_arn}/*/*"
}

resource "aws_api_gateway_stage" "stage" {
  deployment_id = aws_api_gateway_deployment.deployment.id
  rest_api_id   = aws_api_gateway_rest_api.math_api.id
  stage_name    = "dev"
}

resource "aws_api_gateway_method_settings" "settings" {
  rest_api_id = aws_api_gateway_rest_api.math_api.id
  stage_name  = aws_api_gateway_stage.stage.stage_name
  method_path = "*/*"

  settings {
    metrics_enabled = true
    logging_level   = "INFO"
  }
}

