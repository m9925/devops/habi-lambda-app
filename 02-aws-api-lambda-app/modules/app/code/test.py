import json
import unittest
import api

class calculationTest(unittest.TestCase):

    def setUp(self):
        self.event = {"action": "square", "number": 5}

    def test_lambda_handler_square(self):
        self.event = {"action": "square", "number": 5}
        result = api.lambda_handler(self.event,'')
        expected_response = {"result": 25}
        self.assertEqual(result, expected_response)


    def test_lambda_handler_increment(self):
        self.event = {"action": "increment", "number": 5}
        result = api.lambda_handler(self.event,'')
        expected_response = {"result": 6}
        self.assertEqual(result, expected_response)

    def test_lambda_handler_decrement(self):
        self.event = {"action": "decrement", "number": 5}
        result = api.lambda_handler(self.event,'')
        expected_response = {"result": 4}
        self.assertEqual(result, expected_response)

    def test_lambda_handler_square_root(self):
        self.event = {"action": "square root", "number": 25}
        result = api.lambda_handler(self.event,'')
        expected_response = {"result": 5}
        self.assertEqual(result, expected_response)

    

