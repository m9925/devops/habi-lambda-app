terraform {
    backend "s3" {
        bucket = "domus-terraform-backend"
        key    = "dev"
        region = "us-east-2"
        encrypt = true
        kms_key_id = "arn:aws:kms:us-east-2:415861705560:key/76f4d184-8740-4f81-83f8-2481a2f87af9"
    }
}